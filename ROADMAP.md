### 2.2

- **0. Write( or translate ) detailed documentation for building PiSi packages.**
- **1. Write( or fork ) simple and minimalistic text-ui system installer.**
- **2. Prepare default application and preferences for iso.**
- **3. Build missing packages from source repos.**
- **4. Create live installation iso.**

