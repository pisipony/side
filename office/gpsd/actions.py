#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools
from pisi.actionsapi import pisitools
from pisi.actionsapi import scons
from pisi.actionsapi import get

def setup():
	shelltools.export("PYTHON", "/usr/bin/python3")
	scons.configure("prefix=/usr shared=True")

def build():
	scons.make()

def install():
#	scons.install()
	shelltools.system("scons udev-install")

	# We're using conf.d instead of sysconfig
#	pisitools.dosed("gpsd.hotplug.wrapper", "sysconfig\/", "conf.d/")

	# Install UDEV files
#	pisitools.insinto("/lib/udev/rules.d", "gpsd.rules", "99-gpsd.rules")
#	pisitools.dobin("gpsd.hotplug", "/lib/udev")
#	pisitools.dobin("gpsd.hotplug.wrapper", "/lib/udev")

	# Fix permissions
#	shelltools.chmod("%s/usr/lib/%s/site-packages/gps/gps.py" % (get.installDIR(), get.curPYTHON()))

	pisitools.dodoc("AUTHORS", "COPYING", "NEWS", "README.adoc", "SUPPORT.adoc", "TODO")

