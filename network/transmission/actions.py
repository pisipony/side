#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get

j = "--enable-cli \
     --enable-utp \
     --enable-lightweight \
     --enable-daemon \
     --disable-static \
     --disable-mac \
     --with-crypto=openssl \
     --without-systemd \
    "

def setup():
	autotools.configure(j)

def build():
	autotools.make()

def install():
	# gtk
	autotools.rawInstall("-C gtk DESTDIR=%s" % get.installDIR())
	autotools.rawInstall("-C po DESTDIR=%s" % get.installDIR())

	# cli, web, deamon
	for _dir in ["cli", "web", "utils"]:
		autotools.rawInstall("-C %s DESTDIR=%s" % (_dir, get.installDIR()))

	# For daemon config files.
	pisitools.dodir("/etc/transmission-daemon")

	pisitools.dodoc("AUTHORS", "COPYING", "NEWS.md", "README.md")

