### My priorities:

- **No splitting packages.**
- **Strict file formatting for building packages.**
- **Extra focusing to development user-end building toolchain (PiSiDo, gonullu, docker).**
- **Minimal TUI system installer.**

