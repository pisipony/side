#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get

def setup():
	cmaketools.configure("-DCMAKE_BUILD_TYPE=Release", installPrefix="/usr")

def build():
	cmaketools.make()

def install():
	cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "CHANGES", "CODE_OF_CONDUCT.md", "COMPLIANCE", "CONTRIBUTING.md", "COPYING", "INSTALL", "NEWS", "PLATFORMS", "README.md", "THANKS", "TODO", "VERSION")

